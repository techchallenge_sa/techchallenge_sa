# Requirements
## Redis
As application uses Redis cache to test the app please install locally (Redis should use port `6379`) or update `redisCacheClient` section `Web.config`. Keep in mind that web app will flush the database on startup.
Easiest way to install Redis is to use Chocolatey and run `choco install redis-64`. After installing Redis run `redis-server.exe`.
# Information
## Error handling
Doesn't exist. Most actions will fail if Redis is not available. `TotalCustomerBets` API call will fail if customer id doesn't exist.
## Website
At the moment the website will load data before it can be opened. As a result it delay first start for few seconds (or maybe minutes if your internet connection is slow).
## API
- http://localhost:{port}/api/Data/Customers
- http://localhost:{port}/api/Data/RiskyCustomers
- http://localhost:{port}/api/Data/TotalBets
- http://localhost:{port}/api/Data/TotalCustomerBets/{customer-id}