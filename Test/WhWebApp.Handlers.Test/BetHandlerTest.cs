﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WhWebApp.Common;
using WhWebApp.Messages;

namespace WhWebApp.Handlers.Test
{
    [TestClass]
    public class BetHandlerTest
    {
        [TestMethod]
        public void BetHandler_Handle_ShouldPublishTwoMessages()
        {
            var customerId = 1;
            var stake = 123m;
            var horseId = 2;
            var raceId = 3;
            var publisher = new Mock<IPublisher>(MockBehavior.Strict);
            publisher.Setup(m => m.Publish(It.Is<CustomerBetMessage>(message => message.Bet == stake && message.CustomerId == customerId)));
            publisher.Setup(m => m.Publish(It.Is<RaceBetMessage>(message => message.Bet == stake && message.HorseId == horseId && message.RaceId == raceId)));
            var handler = new BetHandler(publisher.Object);
            var betMessage = new BetMessage
            {
                CustomerId = customerId,
                Stake = stake,
                HorseId = horseId,
                RaceId = raceId
            };
            handler.Handle(betMessage);
            publisher.Verify();
        }
    }
}