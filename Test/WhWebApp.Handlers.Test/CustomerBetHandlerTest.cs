﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WhWebApp.Common;
using WhWebApp.Domain;
using WhWebApp.Messages;

namespace WhWebApp.Handlers.Test
{
    [TestClass]
    public class CustomerBetHandlerTest
    {
        [TestMethod]
        public void CustomerBetHandler_Handle_WhenCustomerTotalBetIsLessThan200_ShouldPublishMessage()
        {
            var customerId = 1;
            var name = "test";
            var publisher = new Mock<IPublisher>(MockBehavior.Strict);
            var storage = new Mock<IStorage>();
            storage.Setup(m => m.Get<Customer>(customerId)).Returns(new Customer { Id = customerId, TotalBets = 170, Name = name });
            var handler = new CustomerBetHandler(storage.Object, publisher.Object);
            var betMessage = new CustomerBetMessage
            {
                CustomerId = customerId,
                Bet = 20
            };
            handler.Handle(betMessage);
            publisher.Verify();
        }

        [TestMethod]
        public void CustomerBetHandler_Handle_WhenCustomerTotalBetBecomesOver200_ShouldPublishMessage()
        {
            var customerId = 1;
            var name = "test";
            var publisher = new Mock<IPublisher>(MockBehavior.Strict);
            var storage = new Mock<IStorage>();
            publisher.Setup(m => m.Publish(It.Is<RiskyCustomerMessage>(message => message.Id == customerId && message.Name == name && message.TotalBets == 210m)));
            storage.Setup(m => m.Get<Customer>(customerId)).Returns(new Customer {Id = customerId, TotalBets = 190, Name = name});
            var handler = new CustomerBetHandler(storage.Object, publisher.Object);
            var betMessage = new CustomerBetMessage
            {
                CustomerId = customerId,
                Bet = 20
            };
            handler.Handle(betMessage);
            publisher.Verify();
        }

        [TestMethod]
        public void CustomerBetHandler_Handle_ShouldReplaceExistingValue()
        {
            var customerId = 1;
            var name = "test";
            var publisher = new Mock<IPublisher>();
            var storage = new Mock<IStorage>(MockBehavior.Strict);
            storage.Setup(m => m.Get<Customer>(customerId)).Returns(new Customer { Id = customerId, TotalBets = 170m, Name = name });
            storage.Setup(m => m.Replace(It.Is<Customer>(entity => entity.Name == name && entity.TotalBets == 190m && entity.Id == customerId)));
            var handler = new CustomerBetHandler(storage.Object, publisher.Object);
            var betMessage = new CustomerBetMessage
            {
                CustomerId = customerId,
                Bet = 20
            };
            handler.Handle(betMessage);
            storage.Verify();
        }
    }
}