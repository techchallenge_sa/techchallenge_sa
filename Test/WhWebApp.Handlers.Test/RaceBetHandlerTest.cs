﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WhWebApp.Common;
using WhWebApp.Domain;
using WhWebApp.Messages;

namespace WhWebApp.Handlers.Test
{
    [TestClass]
    public class RaceBetHandlerTest
    {
        [TestMethod]
        public void RaceBetHandler_Handle_RaceWithNoHorses_ShouldAddNewHorse()
        {
            var raceId = 1;
            var horseId = 2;
            var bet = 20m;
            var storage = new Mock<IStorage>(MockBehavior.Strict);
            storage.Setup(m => m.Get<Race>(raceId)).Returns(new Race { Id = raceId, TotalBets = 170m, Horses = new List<Horse>()});
            storage.Setup(m => m.Replace(It.Is<Race>(entity => entity.Horses.Any(h => h.Id == horseId && h.Bets == 1 && h.TotalBets == bet))));
            var handler = new RaceBetHandler(storage.Object);
            var betMessage = new RaceBetMessage
            {
                RaceId = raceId,
                HorseId = horseId,
                Bet = bet
            };
            handler.Handle(betMessage);
            storage.Verify();
        }

        [TestMethod]
        public void RaceBetHandler_Handle_RaceWithHorses_ShouldUpdateExistingHorse()
        {
            var raceId = 1;
            var horseId = 2;
            var bet = 20m;
            var totalBets = 30m;
            var storage = new Mock<IStorage>(MockBehavior.Strict);
            storage.Setup(m => m.Get<Race>(raceId)).Returns(new Race { Id = raceId, TotalBets = totalBets, Horses = new List<Horse> {new Horse {Id = horseId, TotalBets = totalBets, Bets = 1}} });
            storage.Setup(m => m.Replace(It.Is<Race>(entity => entity.Horses.Any(h => h.Id == horseId && h.Bets == 2 && h.TotalBets == bet + totalBets))));
            var handler = new RaceBetHandler(storage.Object);
            var betMessage = new RaceBetMessage
            {
                RaceId = raceId,
                HorseId = horseId,
                Bet = bet
            };
            handler.Handle(betMessage);
            storage.Verify();
        }

        [TestMethod]
        public void RaceBetHandler_Handle_RaceDoesntExist_ShouldCreateNewRace()
        {
            var raceId = 1;
            var horseId = 2;
            var bet = 20m;
            var storage = new Mock<IStorage>(MockBehavior.Strict);
            storage.Setup(m => m.Get<Race>(raceId)).Returns((Race)null);
            storage.Setup(m => m.Add(It.Is<Race>(entity => entity.Id == raceId && entity.TotalBets == bet && entity.Horses.Any(h => h.Id == horseId && h.Bets == 1 && h.TotalBets == bet))));
            var handler = new RaceBetHandler(storage.Object);
            var betMessage = new RaceBetMessage
            {
                RaceId = raceId,
                HorseId = horseId,
                Bet = bet
            };
            handler.Handle(betMessage);
            storage.Verify();
        }
    }
}