﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WhWebApp.Common;
using WhWebApp.Domain;
using WhWebApp.Messages;

namespace WhWebApp.Handlers.Test
{
    [TestClass]
    public class RiskyCustomerHandlerTest
    {
        [TestMethod]
        public void RiskyCustomerHandler_Handle_RaceWithHorses_ShouldUpdateExistingHorse()
        {
            var customerId = 1;
            var bet = 20m;
            var totalBets = 30m;
            var storage = new Mock<IStorage>(MockBehavior.Strict);
            storage.Setup(m => m.Get<RiskyCustomer>(customerId)).Returns(new RiskyCustomer {Id = customerId, TotalBets = bet});
            storage.Setup(m => m.Replace(It.Is<RiskyCustomer>(entity => entity.Id == customerId && entity.TotalBets == totalBets)));
            var handler = new RiskyCustomerHandler(storage.Object);
            var riskyCustomerMessage = new RiskyCustomerMessage
            {
                Id = customerId,
                TotalBets = totalBets
            };
            handler.Handle(riskyCustomerMessage);
            storage.Verify();
        }

        [TestMethod]
        public void RiskyCustomerHandler_Handle_RaceDoesntExist_ShouldCreateNewRace()
        {
            var customerId = 1;
            var bet = 20m;
            var storage = new Mock<IStorage>(MockBehavior.Strict);
            storage.Setup(m => m.Get<RiskyCustomer>(customerId)).Returns((RiskyCustomer)null);
            storage.Setup(m => m.Add(It.Is<RiskyCustomer>(entity => entity.Id == customerId && entity.TotalBets == bet)));
            var handler = new RiskyCustomerHandler(storage.Object);
            var riskyCustomerMessage = new RiskyCustomerMessage
            {
                Id = customerId,
                TotalBets = bet
            };
            handler.Handle(riskyCustomerMessage);
            storage.Verify();
        }
    }
}