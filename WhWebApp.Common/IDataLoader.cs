﻿namespace WhWebApp.Common
{
    public interface IDataLoader
    {
        void Load();
    }
}