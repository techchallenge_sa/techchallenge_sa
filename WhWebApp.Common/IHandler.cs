﻿namespace WhWebApp.Common
{
    public interface IHandler<in TMessage>
    {
        void Handle(TMessage message);
    }
}