﻿namespace WhWebApp.Common
{
    public interface IPublisher
    {
        void Publish<T>(T message);
    }
}