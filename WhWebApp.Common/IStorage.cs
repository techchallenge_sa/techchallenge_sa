﻿using System.Collections.Generic;

namespace WhWebApp.Common
{
    public interface IStorage
    {
        IEnumerable<T> GetAll<T>() where T : DomainObject;
        T Get<T>(int id) where T : DomainObject;
        void Replace<T>(T newValue) where T : DomainObject;
        void Add<T>(T value) where T : DomainObject;
        void FlushDb();
    }
}