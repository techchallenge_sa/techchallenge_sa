﻿using System;

namespace WhWebApp.Common
{
    public interface ISubscriber
    {
        void Subscribe<T>(Action<T> action);
        void Unsubscribe<T>(Action<T> action);
    }
}