﻿using System;
using System.Threading.Tasks;
using WhWebApp.Common;

namespace WhWebApp.Data
{
    public class DataLoader : IDataLoader
    {
        private readonly IPublisher _publisher;
        private readonly IEventsSource _eventsSource;

        public DataLoader(IPublisher publisher, IEventsSource eventsSource)
        {
            _publisher = publisher ?? throw new ArgumentNullException(nameof(publisher));
            _eventsSource = eventsSource ?? throw new ArgumentNullException(nameof(eventsSource));
        }

        public void Load()
        {
            // data is loaded synchronously so it will be ready before UI is loaded
            // this can be a problem if data endpoints are slow/not awailable
            // it would be bettr to load data asynchronously and use SignalR to notify client when data is ready
            Task.WaitAll(LoadBets(), LoadCustomers(), LoadRaces());
        }

        private async Task LoadRaces()
        {
            var raceMessages = await _eventsSource.GetRaceMessages();
            foreach (var raceMessage in raceMessages)
            {
                _publisher.Publish(raceMessage);
            }
        }

        private async Task LoadCustomers()
        {
            var customerMessages = await _eventsSource.GetCustomerMessages();
            foreach (var customerMessage in customerMessages)
            {
                _publisher.Publish(customerMessage);
            }
        }

        private async Task LoadBets()
        {
            var betMessages = await _eventsSource.GetBetMessages();
            foreach (var betMessage in betMessages)
            {
                _publisher.Publish(betMessage);
            }
        }
    }
}