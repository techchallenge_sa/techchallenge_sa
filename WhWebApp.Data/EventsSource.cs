﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using WhWebApp.Messages;

namespace WhWebApp.Data
{
    public class EventsSource : IEventsSource
    {
        private const string BaseUrl = "https://whatech-customerbets.azurewebsites.net/";
        private const string Name = "sa";

        public async Task<List<CustomerMessage>> GetCustomerMessages()
        {
            return await BaseUrl
                .AppendPathSegment("api")
                .AppendPathSegment("GetCustomers")
                .SetQueryParams(new { name = Name })
                .GetJsonAsync<List<CustomerMessage>>();
        }

        public async Task<List<BetMessage>> GetBetMessages()
        {
            return await BaseUrl
                .AppendPathSegment("api")
                .AppendPathSegment("GetBetsV2")
                .SetQueryParams(new { name = Name })
                .GetJsonAsync<List<BetMessage>>();
        }

        public async Task<List<RaceMessage>> GetRaceMessages()
        {
            return await BaseUrl
                .AppendPathSegment("api")
                .AppendPathSegment("GetRaces")
                .SetQueryParams(new { name = Name })
                .GetJsonAsync<List<RaceMessage>>();
        }
    }
}