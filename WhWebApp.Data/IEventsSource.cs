﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WhWebApp.Messages;

namespace WhWebApp.Data
{
    public interface IEventsSource
    {
        Task<List<CustomerMessage>> GetCustomerMessages();
        Task<List<BetMessage>> GetBetMessages();
        Task<List<RaceMessage>> GetRaceMessages();
    }
}