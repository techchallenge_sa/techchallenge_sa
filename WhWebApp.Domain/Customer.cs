﻿using WhWebApp.Common;

namespace WhWebApp.Domain
{
    public class Customer: DomainObject
    {
        public string Name { get; set; }
        public decimal TotalBets { get; set; } = 0;
        public bool IsRisky => TotalBets > 200;
    }
}