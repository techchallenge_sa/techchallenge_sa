﻿using WhWebApp.Common;

namespace WhWebApp.Domain
{
    public class Horse: DomainObject
    {
        public string Name { get; set; }
        public int Bets { get; set; }
        public decimal Odds { get; set; }
        public decimal TotalBets { get; set; }
    }
}