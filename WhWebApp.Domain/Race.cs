﻿using System.Collections.Generic;
using WhWebApp.Common;

namespace WhWebApp.Domain
{
    public class Race: DomainObject
    {
        public RaceStatus Status { get; set; }
        public decimal TotalBets { get; set; } = 0;
        public List<Horse> Horses { get; set; } = new List<Horse>();
    }
}