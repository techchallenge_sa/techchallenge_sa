﻿namespace WhWebApp.Domain
{
    public enum RaceStatus
    {
        Completed,
        InProgress,
        Pending
    }
}