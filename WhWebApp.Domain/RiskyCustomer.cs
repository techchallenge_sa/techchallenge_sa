﻿using WhWebApp.Common;

namespace WhWebApp.Domain
{
    public class RiskyCustomer: DomainObject
    {
        public string Name { get; set; }
        public decimal TotalBets { get; set; } = 0;
    }
}