﻿using System;
using WhWebApp.Common;
using WhWebApp.Messages;

namespace WhWebApp.Handlers
{
    public class BetHandler : IHandler<BetMessage>
    {
        private readonly IPublisher _publisher;

        public BetHandler(IPublisher publisher)
        {
            _publisher = publisher ?? throw new ArgumentNullException(nameof(publisher));
        }

        public void Handle(BetMessage message)
        {
            _publisher.Publish(new RaceBetMessage
            {
                HorseId = message.HorseId,
                RaceId = message.RaceId,
                Bet = message.Stake
            });
            _publisher.Publish(new CustomerBetMessage
            {
                CustomerId = message.CustomerId,
                Bet = message.Stake
            });
        }
    }
}