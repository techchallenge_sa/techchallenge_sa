﻿using System;
using WhWebApp.Common;
using WhWebApp.Domain;
using WhWebApp.Messages;

namespace WhWebApp.Handlers
{
    public class CustomerBetHandler : IHandler<CustomerBetMessage>
    {
        private readonly IStorage _storage;
        private readonly IPublisher _publisher;

        public CustomerBetHandler(IStorage storage, IPublisher publisher)
        {
            _publisher = publisher ?? throw new ArgumentNullException(nameof(publisher));
            _storage = storage ?? throw new ArgumentNullException(nameof(storage));
        }

        public void Handle(CustomerBetMessage message)
        {
            //TODO add transaction
            var customer = _storage.Get<Customer>(message.CustomerId);
            if (customer == null)
            {
                customer = new Customer {Id = message.CustomerId, TotalBets = message.Bet};
                _storage.Add(customer);
            }
            else
            {
                customer.TotalBets += message.Bet;
                _storage.Replace(customer);
            }

            if (customer.IsRisky)
            {
                _publisher.Publish(new RiskyCustomerMessage
                {
                    Id = customer.Id,
                    Name = customer.Name,
                    TotalBets = customer.TotalBets
                });
            }
        }
    }
}