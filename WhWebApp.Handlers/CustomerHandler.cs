﻿using System;
using WhWebApp.Common;
using WhWebApp.Domain;
using WhWebApp.Messages;

namespace WhWebApp.Handlers
{
    public class CustomerHandler : IHandler<CustomerMessage>
    {
        private readonly IStorage _storage;

        public CustomerHandler(IStorage storage)
        {
            _storage = storage ?? throw new ArgumentNullException(nameof(storage));
        }

        public void Handle(CustomerMessage message)
        {
            //TODO reduce duplication
            //TODO add transaction
            var customer = _storage.Get<Customer>(message.Id);
            if (customer == null)
            {
                customer = new Customer {Id = message.Id, Name = message.Name};
                _storage.Add(customer);
            }
            else
            {
                customer.Name = message.Name;
                _storage.Replace(customer);
            }
        }
    }
}