﻿using System;
using System.Collections.Generic;
using System.Linq;
using WhWebApp.Common;
using WhWebApp.Domain;
using WhWebApp.Messages;

namespace WhWebApp.Handlers
{
    public class RaceBetHandler : IHandler<RaceBetMessage>
    {
        private readonly IStorage _storage;

        public RaceBetHandler(IStorage storage)
        {
            _storage = storage ?? throw new ArgumentNullException(nameof(storage));
        }

        public void Handle(RaceBetMessage message)
        {
            //TODO reduce duplication
            //TODO add transaction
            var race = _storage.Get<Race>(message.RaceId);
            if (race == null)
            {
                race = new Race
                {
                    Id = message.RaceId,
                    TotalBets = message.Bet,
                    Horses = new List<Horse> {GetNewHorse(message)}
                };
                _storage.Add(race);
            }
            else
            {
                race.TotalBets += message.Bet;
                var selectedHorse = race.Horses.FirstOrDefault(h => h.Id == message.HorseId);
                if (selectedHorse == null)
                {
                    race.Horses.Add(GetNewHorse(message));
                }
                else
                {
                    selectedHorse.Bets++;
                    selectedHorse.TotalBets += message.Bet;
                }
                _storage.Replace(race);
            }
        }

        private static Horse GetNewHorse(RaceBetMessage message)
        {
            return new Horse {Id = message.HorseId, Bets = 1, TotalBets = message.Bet};
        }
    }
}