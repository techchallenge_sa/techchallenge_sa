﻿using System;
using System.Linq;
using WhWebApp.Common;
using WhWebApp.Domain;
using WhWebApp.Messages;

namespace WhWebApp.Handlers
{
    public class RaceHandler: IHandler<RaceMessage>
    {
        private readonly IStorage _storage;

        public RaceHandler(IStorage storage)
        {
            _storage = storage ?? throw new ArgumentNullException(nameof(storage));
        }

        public void Handle(RaceMessage message)
        {
            //TODO add transaction
            var race = _storage.Get<Race>(message.Id);
            Enum.TryParse(message.Status, true, out RaceStatus status);
            if (race == null)
            {
                race = new Race
                {
                    Status = status,
                    Id = message.Id,
                    Horses = message.Horses.Select(GetNewHorse).ToList()
                };
                _storage.Add(race);
            }
            else
            {
                race.Status = status;
                foreach (var horseMessage in message.Horses)
                {
                    var existingHorse = race.Horses.FirstOrDefault(h => h.Id == horseMessage.Id);
                    if (existingHorse == null)
                    {
                        existingHorse = GetNewHorse(horseMessage);
                        race.Horses.Add(existingHorse);
                    }
                    else
                    {
                        UpdateHorse(existingHorse, horseMessage);
                    }
                }
                _storage.Replace(race);
            }
         }

        private static Horse GetNewHorse(HorseMessage message)
        {
            var horse = new Horse {Id = message.Id};
            UpdateHorse(horse, message);
            return horse;
        }

        private static void UpdateHorse(Horse horse, HorseMessage message)
        {
            horse.Name = message.Name;
            horse.Odds = message.Odds;
        }
    }
}