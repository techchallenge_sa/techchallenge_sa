﻿using System;
using WhWebApp.Common;
using WhWebApp.Domain;
using WhWebApp.Messages;

namespace WhWebApp.Handlers
{
    public class RiskyCustomerHandler : IHandler<RiskyCustomerMessage>
    {
        private readonly IStorage _storage;

        public RiskyCustomerHandler(IStorage storage)
        {
            _storage = storage ?? throw new ArgumentNullException(nameof(storage));
        }

        public void Handle(RiskyCustomerMessage message)
        {
            //TODO reduce duplication
            //TODO add transaction
            var customer = _storage.Get<RiskyCustomer>(message.Id);
            if (customer == null)
            {
                customer = new RiskyCustomer { Id = message.Id, Name = message.Name, TotalBets = message.TotalBets};
                _storage.Add(customer);
            }
            else
            {
                customer.Name = message.Name;
                customer.TotalBets = message.TotalBets;
                _storage.Replace(customer);
            }
        }
    }
}