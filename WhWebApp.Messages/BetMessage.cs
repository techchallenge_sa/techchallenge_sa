﻿using Newtonsoft.Json;

namespace WhWebApp.Messages
{
    /// <summary>
    /// Better not to use same class for both messages and JSON parser.
    /// </summary>
    public class BetMessage
    {
        [JsonProperty(PropertyName = "customerId")]
        public int CustomerId { get; set; }
        [JsonProperty(PropertyName = "horseId")]
        public int HorseId { get; set; }
        [JsonProperty(PropertyName = "raceId")]
        public int RaceId { get; set; }
        [JsonProperty(PropertyName = "stake")]
        public decimal Stake { get; set; }
    }
}