﻿namespace WhWebApp.Messages
{
    public class CustomerBetMessage
    {
        public int CustomerId { get; set; }
        public decimal Bet { get; set; }
    }
}