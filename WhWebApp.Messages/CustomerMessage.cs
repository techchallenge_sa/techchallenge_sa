﻿using Newtonsoft.Json;

namespace WhWebApp.Messages
{
    public class CustomerMessage
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
    }
}