﻿namespace WhWebApp.Messages
{
    public class RaceBetMessage
    {
        public int HorseId { get; set; }
        public int RaceId { get; set; }
        public decimal Bet { get; set; }
    }
}