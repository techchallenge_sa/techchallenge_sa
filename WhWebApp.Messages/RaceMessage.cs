﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace WhWebApp.Messages
{
    public class RaceMessage
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "start")]
        public DateTime Start { get; set; }
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
        [JsonProperty(PropertyName = "horses")]
        public List<HorseMessage> Horses { get; set; } = new List<HorseMessage>();
    }

    public class HorseMessage
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "odds")]
        public decimal Odds { get; set; }
    }
}