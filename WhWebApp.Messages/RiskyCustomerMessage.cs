﻿namespace WhWebApp.Messages
{
    public class RiskyCustomerMessage
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal TotalBets { get; set; } = 0;
    }
}