﻿using System;
using StackExchange.Redis;
using StackExchange.Redis.Extensions.Core;
using WhWebApp.Common;
using ISubscriber = WhWebApp.Common.ISubscriber;

namespace WhWebApp.PubSub
{
    public class RedisPubSub: IPublisher, ISubscriber
    {
        private readonly ICacheClient _cacheClient;
        
        public RedisPubSub(ICacheClient cacheClient)
        {
            _cacheClient = cacheClient ?? throw new ArgumentNullException(nameof(cacheClient));
        }

        public void Publish<T>(T message)
        {
            // come up with some better channel naming
            _cacheClient.Publish(typeof(T).Name, message);
        }

        public void Subscribe<T>(Action<T> action)
        {
            _cacheClient.Subscribe(typeof(T).Name, action);
        }

        public void Unsubscribe<T>(Action<T> action)
        {
            _cacheClient.Unsubscribe(typeof(T).Name, action);
        }
    }
}