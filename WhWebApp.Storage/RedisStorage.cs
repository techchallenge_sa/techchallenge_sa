﻿using System;
using System.Collections.Generic;
using System.Linq;
using StackExchange.Redis.Extensions.Core;
using WhWebApp.Common;

namespace WhWebApp.Storage
{
    public class RedisStorage : IStorage
    {
        private readonly ICacheClient _cacheClient;

        public RedisStorage(ICacheClient cacheClient)
        {
            _cacheClient = cacheClient ?? throw new ArgumentNullException(nameof(cacheClient));
        }

        public IEnumerable<T> GetAll<T>() where T : DomainObject
        {
            //KEYS can be slow on big DB, better use SCAN instead
            var keys = _cacheClient.SearchKeys($"{typeof(T).Name}:*");
            return _cacheClient.GetAll<T>(keys).Select(kvp => kvp.Value);
        }

        public T Get<T>(int id) where T : DomainObject
        {
            return _cacheClient.Get<T>(GetKey<T>(id));
        }

        public void Replace<T>(T newValue) where T : DomainObject
        {
            _cacheClient.Replace(GetKey<T>(newValue.Id), newValue);
        }

        public void Add<T>(T value) where T : DomainObject
        {
            _cacheClient.Add(GetKey<T>(value.Id), value);
        }

        public void FlushDb()
        {
            _cacheClient.FlushDb();
        }

        // Remove is not implemented

        private static string GetKey<T>(int id) where T : DomainObject
        {
            return $"{typeof(T).Name}:{id}";
        }
    }
}