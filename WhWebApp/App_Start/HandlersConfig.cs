﻿using Unity;
using WhWebApp.Common;
using WhWebApp.Messages;

namespace WhWebApp
{
    public class HandlersConfig
    {
        public static void RegisterAllHandlers()
        {
            var subscriber = UnityConfig.Container.Resolve<ISubscriber>();
            //should find and activate all handlers automatically
            subscriber.Subscribe<BetMessage>(UnityConfig.Container.Resolve<IHandler<BetMessage>>().Handle);
            subscriber.Subscribe<CustomerBetMessage>(UnityConfig.Container.Resolve<IHandler<CustomerBetMessage>>().Handle);
            subscriber.Subscribe<CustomerMessage>(UnityConfig.Container.Resolve<IHandler<CustomerMessage>>().Handle);
            subscriber.Subscribe<RaceBetMessage>(UnityConfig.Container.Resolve<IHandler<RaceBetMessage>>().Handle);
            subscriber.Subscribe<RaceMessage>(UnityConfig.Container.Resolve<IHandler<RaceMessage>>().Handle);
            subscriber.Subscribe<RiskyCustomerMessage>(UnityConfig.Container.Resolve<IHandler<RiskyCustomerMessage>>().Handle);
        }
    }
}