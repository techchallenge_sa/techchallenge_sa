using StackExchange.Redis.Extensions.Core;
using StackExchange.Redis.Extensions.Newtonsoft;
using System;
using Unity;
using Unity.Lifetime;
using WhWebApp.Common;
using WhWebApp.Data;
using WhWebApp.Handlers;
using WhWebApp.Messages;
using WhWebApp.PubSub;
using WhWebApp.Storage;

namespace WhWebApp
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container

        private static Lazy<IUnityContainer> container =
            new Lazy<IUnityContainer>(() =>
            {
                var container = new UnityContainer();
                RegisterTypes(container);
                return container;
            });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;

        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            //data is shared between all users
            container.RegisterInstance<ICacheClient>(new StackExchangeRedisCacheClient(new NewtonsoftSerializer()));
            container.RegisterType<IStorage, RedisStorage>();
            container.RegisterType<IPublisher, RedisPubSub>();
            container.RegisterType<ISubscriber, RedisPubSub>();
            container.RegisterType<IEventsSource, EventsSource>();
            container.RegisterType<IDataLoader, DataLoader>();
            //should find and register all handlers in the project automatically
            container.RegisterType<IHandler<BetMessage>, BetHandler>(new ContainerControlledLifetimeManager());
            container.RegisterType<IHandler<CustomerBetMessage>, CustomerBetHandler>(new ContainerControlledLifetimeManager());
            container.RegisterType<IHandler<CustomerMessage>, CustomerHandler>(new ContainerControlledLifetimeManager());
            container.RegisterType<IHandler<RaceBetMessage>, RaceBetHandler>(new ContainerControlledLifetimeManager());
            container.RegisterType<IHandler<RaceMessage>, RaceHandler>(new ContainerControlledLifetimeManager());
            container.RegisterType<IHandler<RiskyCustomerMessage>, RiskyCustomerHandler>(new ContainerControlledLifetimeManager());
        }
    }
}