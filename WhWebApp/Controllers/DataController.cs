﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using WhWebApp.Common;
using WhWebApp.Domain;

namespace WhWebApp.Controllers
{
    public class DataController : ApiController
    {
        [System.Web.Http.Route("api/Data/Customers")]
        [System.Web.Http.HttpGet]
        public JsonResult<List<Customer>> Customers()
        {
            return Json(DependencyResolver.Current.GetService<IStorage>().GetAll<Customer>().OrderBy(c => c.Id).ToList());
        }

        [System.Web.Http.Route("api/Data/RiskyCustomers")]
        [System.Web.Http.HttpGet]
        public JsonResult<List<RiskyCustomer>> RiskyCustomers()
        {
            return Json(DependencyResolver.Current.GetService<IStorage>().GetAll<RiskyCustomer>().OrderBy(c => c.Id).ToList());
        }

        [System.Web.Http.Route("api/Data/TotalBets")]
        [System.Web.Http.HttpGet]
        public JsonResult<decimal> TotalBets()
        {
            return Json(DependencyResolver.Current.GetService<IStorage>().GetAll<Customer>().Sum(c => c.TotalBets));
        }

        [System.Web.Http.Route("api/Data/TotalCustomerBets/{id}")]
        [System.Web.Http.HttpGet]
        public JsonResult<decimal> TotalCustomerBets(int id)
        {
            return Json(DependencyResolver.Current.GetService<IStorage>().Get<Customer>(id).TotalBets);
        }
    }
}
