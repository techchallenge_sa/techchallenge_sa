﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WhWebApp.Common;
using WhWebApp.Domain;
using WhWebApp.ViewModels;

namespace WhWebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IStorage _storage;

        public HomeController(IStorage storage)
        {
            _storage = storage ?? throw new ArgumentNullException(nameof(storage));
        }

        public ActionResult Index()
        {
            return View(_storage.GetAll<Race>().OrderBy(r => r.Id));
        }
    }
}