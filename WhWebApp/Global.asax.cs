﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WhWebApp.Common;

namespace WhWebApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            HandlersConfig.RegisterAllHandlers();

            //flushing Redis DB as I did not implement correct entity merge
            DependencyResolver.Current.GetService<IStorage>().FlushDb();
            DependencyResolver.Current.GetService<IDataLoader>().Load();
        }
    }
}
