﻿using WhWebApp.Domain;

namespace WhWebApp.ViewModels
{
    public class RaceViewModel
    {
        public RaceStatus Status { get; set; }
        public decimal TotalBets { get; set; }
    }
}